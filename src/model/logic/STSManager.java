package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.sql.Time;
import java.util.Iterator;

import api.ISTSManager;
import model.vo.VORoute;
import model.vo.VOStop;
import model.vo.VOStopTimes;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.RingList;

public class STSManager implements ISTSManager {

	DoubleLinkedList<VORoute> routes = new DoubleLinkedList<VORoute>();
	RingList<VOStop> stops = new RingList<VOStop>();
	DoubleLinkedList<VOStopTimes> stopTimes = new DoubleLinkedList<VOStopTimes>();
	RingList<VOTrip> trips = new RingList<VOTrip>();
	
	@Override
	public void loadRoutes(String routesFile) {
		// TODO Auto-generated method stub
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(routesFile)));
			reader.readLine();
			String line = reader.readLine();
			while( line != null )
			{
				String data[] = line.split(",");

				int id = Integer.parseInt(data[0]);
				String agencyId = data[1];
				short routeNum = Short.parseShort(data[2]);
				String routeName = data[3];
				String routeDesc = data[4];
				URL routeUrl = new URL(data[5]);

				routes.add(new VORoute(id, agencyId, routeNum, routeName, routeDesc, routeUrl));
				
				line = reader.readLine();
			}

		} catch (Exception e) {
			System.out.println(e);
		}	
	}

	@Override
	public void loadTrips(String tripsFile) {
		// TODO Auto-generated method stub
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(tripsFile)));
			reader.readLine();
			String line = reader.readLine();
			while( line != null )
			{
				String data[] = line.split(",");

				int routeId = Integer.parseInt(data[0]);
				int serviceId = Integer.parseInt(data[1]);
				int tripId = Integer.parseInt(data[2]);
				String headSing = data[3];
				String tripShortName = data[4];
				int directionId = Integer.parseInt(data[3]);
				int blockId = Integer.parseInt(data[4]);
				int shapeId = Integer.parseInt(data[5]);
				int bikeAllowed = Integer.parseInt(data[6]);
				int wheelchairFriendly = Integer.parseInt(data[7]);

				trips.add(new VOTrip(routeId, serviceId, tripId, headSing, 
						tripShortName, directionId, blockId, shapeId, bikeAllowed, wheelchairFriendly));
				
				line = reader.readLine();
			}

		} catch (Exception e) {
			// TODO: handle exception
		}	
	}

	@Override
	public void loadStopTimes(String stopTimesFile) {
		// TODO Auto-generated method stub
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(stopTimesFile)));
			reader.readLine();
			String line = reader.readLine();
			while( line != null )
			{
				String data[] = line.split(",");

				int id = Integer.parseInt(data[0]);

				String arrivalTimeData[] = data[1].split(":");	
				Time arrival = new Time(Integer.parseInt(arrivalTimeData[0]), Integer.parseInt(arrivalTimeData[1]), Integer.parseInt(arrivalTimeData[2]));

				String departureTimeData[] = data[2].split(":");
				Time departure = new Time(Integer.parseInt(departureTimeData[0]), Integer.parseInt(departureTimeData[1]), Integer.parseInt(departureTimeData[2]));

				int stopId = Integer.parseInt(data[3]);
				String stopSequence = data[4];
				String stopSign = data[5];
				int pickUp = Integer.parseInt(data[6]);
				int drop = Integer.parseInt(data[7]);
				double distance;
				if(!data[8].isEmpty()) 
				{
					distance = Double.parseDouble(data[8]);
				}
				else distance = 0;
				stopTimes.add(new VOStopTimes(id, arrival, departure, stopId, stopSequence, stopSign, pickUp, drop, distance));

				line = reader.readLine();
			}

		} catch (Exception e) {

		}

	}

	@Override
	public void loadStops(String stopsFile) {
		// TODO Auto-generated method stub
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(stopsFile)));
			reader.readLine();
			String line = reader.readLine();
			while( line != null )
			{
				String data[] = line.split(",");

				int id = Integer.parseInt(data[0]);
				int code= Integer.parseInt(data[1]);
				String name = data[2];
				String stopDesc = data[3];
				double stopLat = Double.parseDouble(data[4]);
				double stopLon = Double.parseDouble(data[5]);
				String stopZone = data[6];
				URL stopUrl = new URL(data[7]);
				int locationType = Integer.parseInt(data[8]);
				String parentStation = data[9];

				stops.add(new VOStop(id, code, name, stopDesc, stopLat, stopLon, stopZone, stopUrl, locationType, parentStation));

				line = reader.readLine();
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public IList<VORoute> routeAtStop(String stopName)
	{
		VOStopTimes actual = stopTimes.darPrimero();
		
		VOTrip actualTrip = trips.darPrimero();
		
		VORoute actualRoute = routes.darPrimero();
		
		DoubleLinkedList<VORoute> returnRoutes = new DoubleLinkedList<VORoute>();

		
		
		for (int i = 0; i < stops.size(); i++)
		{
			 VOStop objeto = stops.get(i);
			 
			 while(objeto.getName().equalsIgnoreCase(stopName))
			 {
				int stopId = actual.getStopId();
				
				while(actualTrip.getServiceId() == stopId)
				{
					int tripService = actualTrip.getServiceId();
					
					while(tripService == actualRoute.id())
					{
						returnRoutes.add(actualRoute);
						actualRoute = actualRoute.darSiguiente();
					}
					actualTrip = actualTrip.darSiguiente();
				}
				actual = actual.darSiguiente();
			 }
		}
		
		return returnRoutes;
	}

	@Override
	public IList<VOStop> stopsRoute(String routeName, String direction)
	{
		// TODO Auto-generated method stub
		return null;
	}

}
