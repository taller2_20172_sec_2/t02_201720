package model.vo;

import java.sql.Time;

public class VOStopTimes {

	private int tripId;
	private Time arrivalTime;
	private Time departureTime;
	private int stopId;
	private String stopSequence;
	private String stopHeadSign;
	private int pickUpType;
	private int dropType;
	private double distanceTraveled;
	private VOStopTimes primero;
	
	public VOStopTimes(int pId, Time pArrivalTime, Time pDepartureTime, int pStopId, String pStopSequence, String pStopSign, int pPickupType, int pDropType, double pDistance )
	{
		tripId = pId;
		arrivalTime = pArrivalTime;
		departureTime = pDepartureTime;
		stopId = pStopId;
		stopSequence = pStopSequence;
		stopHeadSign = pStopSign;
		pickUpType = pPickupType;
		dropType = pDropType;
		distanceTraveled = pDistance;
		primero = null;
	}

	public int getTripId() {
		return tripId;
	}

	public void setTripId(int tripId) {
		this.tripId = tripId;
	}

	public Time getArrivalTime() {
		return arrivalTime;
	}

	

	public Time getDepartureTime() {
		return departureTime;
	}

	

	public int getStopId() {
		return stopId;
	}

	

	public String getStopSequence() {
		return stopSequence;
	}

	

	public String getStopHeadSign() {
		return stopHeadSign;
	}

	

	public int getPickUpType() {
		return pickUpType;
	}

	

	public int getDropType() {
		return dropType;
	}

	

	public double getDistanceTraveled() {
		return distanceTraveled;
	}

	
	
	
}
