package model.vo;

public class VOTrip {

	//route_id,service_id,trip_id,trip_headsign,trip_short_name,direction_id,block_id,shape_id,wheelchair_accessible,bikes_allowed

	private int routeId;
	
	private int serviceId;
	
	private int tripId;
	
	private String headSing;
	
	private String tripShortName;
	
	private int directionId;
	
	private int blockId;
	
	private int shapeId;
	
	private boolean bikeAllowed;
	
	private boolean wheelchair;
	
	public VOTrip(int pRouteID, int pServiceId, int pTripId, String pHeadSign, String pTripShortName, int pDirectionId, int pBlockId, int pShapeId, int pBike, int pWheel)
	{
		routeId = pRouteID;
		serviceId = pServiceId;
		tripId = pTripId;
		headSing = pHeadSign;
		tripShortName = pTripShortName;
		directionId = pDirectionId;
		blockId = pBlockId;
		shapeId = pShapeId;
		if( pBike == 0 ) {
			bikeAllowed = false;
		}
		else bikeAllowed = true;


		if( pWheel == 0 )
		{
			wheelchair = false;
		}

		else wheelchair = true;
	}
	
	public int getRouteId() {
		return routeId;
	}

	public void setRouteId(int routeId) {
		this.routeId = routeId;
	}

	public int getServiceId() {
		return serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	public int getTripId() {
		return tripId;
	}

	public void setTripId(int tripId) {
		this.tripId = tripId;
	}

	public String getHeadSing() {
		return headSing;
	}

	public void setHeadSing(String headSing) {
		this.headSing = headSing;
	}

	public String getTripShortName() {
		return tripShortName;
	}

	public void setTripShortName(String tripShortName) {
		this.tripShortName = tripShortName;
	}

	public int getDirectionId() {
		return directionId;
	}

	public void setDirectionId(int directionId) {
		this.directionId = directionId;
	}

	public int getBlockId() {
		return blockId;
	}

	public void setBlockId(int blockId) {
		this.blockId = blockId;
	}

	public int getShapeId() {
		return shapeId;
	}

	public void setShapeId(int shapeId) {
		this.shapeId = shapeId;
	}

	public boolean isBikeAllowed() {
		return bikeAllowed;
	}


	public boolean isWheelchair() {
		return wheelchair;
	}

	
}
