package model.vo;

import java.net.URL;

/**
 * Representation of a route object
 */
public class VORoute 
{

	
	private int id;
	
	private String agencyID;
	
	private short routeNum;
	
	private String routeName;
	
	private String routeDesc;
	
	private URL routeUrl;
	
	public VORoute(int pId, String pAgencyId, short pRouteNum, String pRouteName, String pRouteDesc, URL pRouteUrl)
	{
		id = pId;
		agencyID = pAgencyId;
		routeNum = pRouteNum;
		routeName = pRouteName;
		routeDesc = pRouteDesc;
		routeUrl = pRouteUrl;
	}
	
	
	
	/**
	 * @return id - Route's id number
	 */
	public int id() {
		// TODO Auto-generated method stub
		return id;
	}

	/**
	 * @return name - route name
	 */
	public String getName() {
		// TODO Auto-generated method stub
		return routeName;
	}

	public String getDesc()
	{
		return routeDesc;
	}
	
	public URL getRouteUrl()
	{
		return routeUrl;
	}
}
