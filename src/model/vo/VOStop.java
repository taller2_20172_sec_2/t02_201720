package model.vo;

import java.net.URL;

/**
 * Representation of a Stop object
 */
public class VOStop {

	private int id;
	private int code;
	private String name;
	private String stopDesc;
	private double stopLat;
	private double stopLon;
	private String stopZone;
	private URL stopUrl;
	private int locationType;
	private String parentStation;
	
	public VOStop(int pId, int pCode, String pName, String pStopDesc, double pStopLat, double pStopLon, String pStopZone, URL pUrl, int pLocType, String pParent)
	{
		id = pId;
		code = pCode;
		name = pName;
		stopDesc = pStopDesc;
		stopLat = pStopLat;
		stopLon = pStopLon;
		stopZone = pStopZone;
		stopUrl = pUrl;
		locationType = pLocType;
		parentStation = pParent;
	}
	
	/**
	 * @return id - stop's id
	 */
	public int getId() {
		// TODO Auto-generated method stub
		return id;
	}

	/**
	 * @return name - stop name
	 */
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}
	
	public String getStopDesc() {
		return stopDesc;
	}
	
	public double getStopLat() {
		return stopLat;
	}


	public double getStopLon() {
		return stopLon;
	}



	public String getStopZone() {
		return stopZone;
	}

	

	public URL getStopUrl() {
		return stopUrl;
	}

	

	public int getLocationType() {
		return locationType;
	}

	

	public String getParentStation() {
		return parentStation;
	}

	
}
