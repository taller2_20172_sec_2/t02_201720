package model.data_structures;


public class ListIterator<T> implements java.util.ListIterator<T> {

	private int size;
	private Node<T> posicion;
	private Node<T> primero;
	private Node<T> ultimo;
	private Node<T> anterior;

	public ListIterator() {
		posicion = null;
		anterior = null;
		size=0;
	}


	public T next() {
		if (!hasNext()) {
			try {
				throw new Exception("Está en el final de la lista");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		anterior = posicion;
		if (posicion == null) {
			posicion = primero;
		} else {
			posicion = posicion.darSiguiente();
		}
		return posicion.data;
	}

	@Override
	public T previous() {
		if (!hasPrevious()) {
			try {
				throw new Exception("Está en el principio de la lista");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		anterior = posicion;
		if (posicion == null) {
			posicion = ultimo;
		} else {
			posicion = posicion.darAnterior();
		}
		return posicion.data;
	}


	public boolean hasNext() {
		if (posicion == null) {
			return primero != null;
		} else {
			return posicion.darSiguiente() != null;
		}
	}

	@Override
	public boolean hasPrevious() {
		if (posicion == null) {
			return ultimo != null;
		} else {
			return posicion.darAnterior() != null;
		}
	}

	@Override
	public void add(T element) {
		if (posicion == null) {
			posicion = primero;
		} else {
			Node<T> nuevoNodo = new Node<T>(element);
			nuevoNodo.data = element;
			nuevoNodo.setSiguiente(posicion.darSiguiente());

			posicion.setSiguiente(nuevoNodo);
			nuevoNodo.setAnterior(posicion); 
			posicion = nuevoNodo;
			if (nuevoNodo.darAnterior() == ultimo) {
				ultimo = nuevoNodo;
			}
			size++;
		}
		anterior = posicion;
	}

	public void remove() {
		if (anterior == posicion) {
			try {
				throw new Exception();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		Node nodoB = posicion;
		anterior.setSiguiente(posicion.darSiguiente());
		if (posicion.darSiguiente() != null) {
			posicion.darSiguiente().setAnterior(anterior);
		}
		nodoB.setSiguiente(nodoB.darAnterior());
		size--;

		posicion = anterior;
	}

	@Override
	public void set(T element) {
		if (posicion == null) {
			try {
				throw new Exception("El nodo es nulo");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		posicion.data = element;
	}



	public int nextIndex() {
		int cont=0;
		Node <T> siguiente = posicion.darSiguiente();
		Node<T> nodoActual = primero;
		while(nodoActual!=null)
		{
			if(nodoActual.equals(siguiente))
			break;
			
			cont++;
			nodoActual=nodoActual.darSiguiente();
		}
		

		return cont;
	}


	public int previousIndex() {
		int cont=0;
		Node <T> anterior = posicion.darAnterior();
		Node<T> nodoActual = primero;
		while(nodoActual!=null)
		{
			if(nodoActual.equals(anterior))
			break;
			
			cont++;
			nodoActual=nodoActual.darSiguiente();
		}
		

		return cont;

	}



}