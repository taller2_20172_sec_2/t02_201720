package model.data_structures;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, addAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T> extends Iterable<T> {
	/**
	 * Agrega un elemento a la lista
	 * @param elem Elemento que va a agregar
	 * @return 
	 */
	void add(T elem);
	
	/**
	 * Agrega un elemento al final de la lista 
	 * @param elem Elemento que va a agregar
	 */
	void addAtEnd(T elem);
	/**
	 * Da el elemento en la posición i
	 * @param i Posición del elemento a recuperar
	 * @return Elemento recuperado de la posición i
	 */
	T getElement(int i);
	/**
	 * Da el elemento en la posición actual
	 * @return Elemento en la posición actual
	 */
	T getCurrentElement();
	/**
	 * Da el tamaño total de la lista
	 * @return Tamaño de la lista
	 */
	Integer getSize();

	/**
	 * Borra el elemento de la posición k
	 * @param k posición del elemento a borrar
	 * @return True si funcionó. False en caso contrario
	 */
	boolean deleteAtK(int k);
	/**
	 * Mueve la posición actual al siguiente elemento
	 */
	void next();
	/**
	 * Mueve la posición actual al anterior elemento
	 */
	void previous();

}