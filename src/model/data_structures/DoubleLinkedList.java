package model.data_structures;

import model.vo.VORoute;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class DoubleLinkedList<T> implements IList<T>{


	//--------------------------------------------------------------
	//Atributos
	//--------------------------------------------------------------
	
	private List<T> lista;
	
	private Node anterior;     
	private Node siguiente;

	private int idActual;
	private int size;

	private Node primero;
	private Node ultimo;


	//--------------------------------------------------------------
	//Constructor
	//--------------------------------------------------------------

	public DoubleLinkedList()
	{
		primero=null;
		idActual=0;
		size=0;
		lista=null;

	}
	

	//--------------------------------------------------------------
	//Metodos
	//--------------------------------------------------------------

	public Node<T> darPrimero()
	{
		return primero;
	}

	public Node<T> darUltimo()
	{
		return ultimo;
	}

	public Node <T> darAnterior()
	{
		return anterior;
	}

	public void agregarAlIncio(Node <T> nuevoNodo)
	{

		if(primero == null)
		{
			primero = nuevoNodo;
			ultimo = primero;
		}
		else
		{
			primero.setAnterior(nuevoNodo);
			nuevoNodo.setSiguiente(primero);
			primero = nuevoNodo;
		}
		size++;
	}
	

	public boolean yaExiste(T elemento)
	{
		if(size==0)
		{
			return false;
		}
		else{
			boolean existe = false;
			if(primero.getData().equals(elemento)){
				return true;
			}
			else{
				Node <T> nActual =primero;

				for(int i=0;i<lista.size();i++)
				{
					if(nActual.getData().equals(elemento))
						existe=true;

				}
			}
			return existe;
		}
	}

	public Iterator iterator() {
		// TODO Auto-generated method stub
		return null;
	}


	public void add(T elem) {
		Node<T> nuevoNodo = new Node <T> (elem);
		if(yaExiste(elem))
		{
			System.out.println("Ya existe un elemento con ese identificador");
			return;
		}
		if(primero == null)
		{
			ultimo = nuevoNodo;
			primero=ultimo;
		}
		else
		{
			Node <T> current = new Node(getCurrentElement());
			Node <T> nextCurrent = current.darSiguiente();
			nuevoNodo.setSiguiente(current.darSiguiente());
			nuevoNodo.setAnterior(current);
			current.setSiguiente(nuevoNodo);
			nextCurrent.setAnterior(nuevoNodo);
		}
		size++;

	}


	public void addAtEnd(T elem) {
		Node<T> nuevoNodo = new Node <T> (elem);
		if(yaExiste(elem))
		{
			System.out.println("Ya existe un elemento con ese identificador");
			return;
		}
		if(primero == null)
		{
			ultimo = nuevoNodo;
			primero=ultimo;
		}
		else
		{
			ultimo.setSiguiente(nuevoNodo);
			nuevoNodo.setSiguiente(null);
			ultimo = nuevoNodo;
		}
		size++;


	}

	public T getElement(int i) {
		T ans=null;
		if(lista.size()==0)
			ans= null;
		else if(i<lista.size())
		{
			ans= lista.get(i);
		}

		return ans;
	}


	public T getCurrentElement() {
		return getElement(idActual);
	}

	public Integer getSize() {
	return lista.size();
	}

	public Node<T> obtnerNodoK(int k)
	{
		Node<T> nodoBucado= null;
		T elem = getElement(k);

			Node<T> primer=primero;
			while(primer!=null)
			{
				if(primer.getData().equals(elem))
				nodoBucado=primer;
				
				primer=primer.siguiente;
			}
			return nodoBucado;
			
	}
	
	public boolean deleteAtK(int k) {
		
		Node <T> nActual=obtnerNodoK(k);
		if(nActual.equals(null))
			return false;
		else{
			
		Node <T> anteriorN = nActual.darAnterior();
		Node <T> siguienteN = nActual.darSiguiente();
		
		anteriorN.setSiguiente(siguienteN);
		siguienteN.setAnterior(anteriorN);
		nActual.setAnterior(null);
		nActual.setSiguiente(null);
		return true;
		}
	}

	
	public void next() {
		idActual+=1;
	}

	
	public void previous() {
		idActual-=1;
	}



}