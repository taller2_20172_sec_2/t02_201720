package model.data_structures;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Iterator;

public class RingList<T> implements IList<T>{
	
	//--------------------------------------------------------------
	//Atributos
	//--------------------------------------------------------------
	private Node<T> primero;
	private int actual;
	private int size;
	

	//--------------------------------------------------------------
	//Constructor
	//--------------------------------------------------------------
	
	public RingList(){
		primero = null;
		size = 0;
		actual = 0;
	}
	
	
	//--------------------------------------------------------------
	//Metodos
	//--------------------------------------------------------------
	
	/**
	 * Agrega un elemento a la lista en la posicion siguiente a la actual
	 * @param elem Elemento que va a agregar
	 */
	public void add(T elem){
		Node<T> agregar = new Node<T>(elem);
		if(yaExiste(elem)){
			System.out.println("Ya existe un elemento con ese identificador");
			return;
		}
		else if(size == 0){
			primero = agregar;
			primero.setAnterior(primero);
			primero.setSiguiente(primero);
		}
		else{
			Node<T> nActual = primero;
			for(int i=0; i<=actual; i++){
				if(i != actual)
					nActual = nActual.darSiguiente();
				else{
					agregar.setSiguiente(nActual.darSiguiente());
					nActual.darSiguiente().setAnterior(agregar);
					nActual.setSiguiente(agregar);
					agregar.setAnterior(nActual);
				}
			}
		}
		size ++;
		actual = (actual+1)%size;
	}
	
	/**
	 * Agrega un elemento al final de la lista	
	 * @param elem Elemento que va a agregar
	 */
	public void addAtEnd(T elem){
		Node<T> agregar = new Node<T>(elem);
		if(yaExiste(elem)){
			System.out.println("Ya existe un elemento con ese identificador");
			return;
		}
		else if(size == 0){
			primero = agregar;
			primero.setSiguiente(agregar);
			primero.setAnterior(agregar);
		}
		else{
			Node<T> nActual = primero;
			for(int i=0; i<size; i++){
				if(nActual.darSiguiente().getData().equals(primero.getData())){
					agregar.setSiguiente(primero);
					primero.setAnterior(primero);
					nActual.setSiguiente(agregar);
					agregar.setAnterior(nActual);
				}
				else
					nActual = nActual.darSiguiente();
			}
		}
		size ++;
		actual = (actual+1)%size;
	}
	
	/**
	 * Da el elemento en la posici�n i
	 * @param i Posici�n del elemento a recuperar.
	 * @return Elemento recuperado de la posici�n i. Retorna null si no hay elementos en la lista
	 */
	public T getElement(int i){
		if(size == 0)
			return null;
		else if(size == 1)
			return primero.getData();
		else{
			Node<T> nActual = primero;
			int pos = i%size;
			for(int j=0; j<=pos; j++){
				if(j != pos)
					nActual = nActual.darSiguiente();
				else
					break;
			}
			return nActual.getData();
		}	
	}
	
	/**
	 * Da el elemento en la posici�n actual
	 * @return Elemento en la posici�n actual. Si no hay elementos retorna null
	 */
	public T getCurrentElement(){
		if(size == 0)
			return null;
		else if(size == 1)
			return primero.getData();
		else{
			Node<T> nActual = primero;
			for(int i=0; i<=actual; i++){
				if(i!=actual)
					nActual = nActual.darSiguiente();
				else
					break;
			}
			return nActual.getData();
		}
	}
	
	/**
	 * Da el tama�o total de la lista
	 * @return Tama�o de la lista
	 */
	public Integer getSize(){
		return size;
	}
	
		
	/**
	 * Borra el elemento de la posici�n k
	 * @param k posici�n del elemento a borrar
	 * @return True si funcion�. False en caso contrario
	 */
	public boolean deleteAtK(int k){
		if(size == 0)
			return false;
		else if(size == 1){
			primero = null;
			size --;
			actual = 0;
			return true;
		}
		else{
			int pos = k%size;
			Node<T> nActual = primero;
			for(int i=0; i<=pos; i++){
				if(i != pos)
					nActual = nActual.darSiguiente();
				else
					break;
			}
			Node<T> ant = nActual.darAnterior();
			Node<T> sig = nActual.darSiguiente();
			nActual.darAnterior().setSiguiente(sig);
			nActual.darSiguiente().setAnterior(ant);
			size --;
			actual = (actual-1)%size;
			return true;
		}
	}
	
	/**
	 * Mueve la posici�n actual al siguiente elemento
	 */
	public void next(){
		actual = (actual+1)%size;
	}
	
	/**
	 * Mueve la posici�n actual al anterior elemento
	 */
	public void previous(){
		actual = (actual-1)%size;
	}
	
	/**
	 * Verifica si el id que entra por par�metro ya existe en la lista
	 * @param pId El identificador buscado
	 * @return True si ya existe dentro de la lista. False en caso contrario
	 */
	public boolean yaExiste(T elemento)
	{
		if(size==0)
		{
			return false;
		}
		else{
			boolean existe = false;
			if(primero.getData().equals(elemento)){
				return true;
			}
			else{
				Node <T> nActual =primero;

				for(int i=0;i<size;i++)
				{
					if(nActual.getData().equals(elemento))
						existe=true;

				}
			}
			return existe;
		}
	}
	
	public Node <T> obtenerUltimo()
	{
		return obtnerNodoK(size-1);
	}

	public Node<T> obtnerNodoK(int k)
	{
		Node<T> nodoBucado= null;
		T elem = getElement(k);

			Node<T> primer=primero;
			while(primer!=null)
			{
				if(primer.getData().equals(elem))
				nodoBucado=primer;
				
				primer=primer.siguiente;
			}
			return nodoBucado;
			
	}


	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}