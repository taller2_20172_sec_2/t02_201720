package testData_structures;

import junit.framework.TestCase;
import model.data_structures.RingList;

public class testRingList<T> extends TestCase {
	private RingList<T> listaEncadenada;

	private void setupEscenario1()
	{
		ringList= new RingList<T>();
	}
	
	private void setupEscenario2()
	{
		try
		{
			ringList = new RingList<>();

			T primerElemento = (T) "Route1";
			T segundoElemento =  (T) "Route2";
			T tercerElemento = (T) "Route3";


			ringList.agregarElementoFinal(primerElemento);
			ringList.agregarElementoFinal(segundoElemento);
			ringList.agregarElementoFinal(tercerElemento);


		}
		catch( Exception e )
		{
			fail("No deber�a lanzar excepci�n.");
		}

	}

	public void testListaEncadenada( )
	{
		setupEscenario1();
		assertEquals("La lista debe encontrarse vac�a.", 0, ringList.darNumeroElementos());
		
		setupEscenario2( );

		assertEquals( "La lista  debe crearse vac�a.", 3, ringList.darNumeroElementos() );
		assertNotNull( "La lista deber�a tener un primer elemento.", ringList.darElemento(0));
		assertEquals( "El primer elemento no es el esperado.", "Route1", ringList.darElemento(0));
		assertEquals( "El segundo elemento no es el esperado.", "Route2", ringList.darElemento(1));
		assertEquals("El enlace entre el primer y �ltimo elemento no es el esperado", "Route3", ringList.darUltimo().darItem().darSiguiente());
		assertEquals( "El ultimo elemento no es el esperado.", "Route3", ringList.darUltimo().darItem());

	}

	public void testDarElementoPosicionActual()
	{
		setupEscenario2();

		assertEquals( "La lista  debe crearse vac�a.", "Route1", ringList.darElementoPosicionActual() );

	}
	public void testProbarDarElemento()
	{
		setupEscenario2();
		assertEquals("El elemento no es el esperado", "Route2", ringList.darElemento(1));
	}

	public void testProbarDarNumeroElementos()
	{
		setupEscenario1();
		assertEquals("La lista debe encontrarse vac�a.", 0, ringList.darNumeroElementos());
		
		setupEscenario2();
		assertEquals("El tama�o no es el esperado", 3, ringList.darNumeroElementos());
	}

	public void testGetNext()
	{
		setupEscenario2();
		if(ringList.darElementoPosicionActual().equals(ringList.darUltimo()))
			assertFalse("No se puede avanzar porque es el ultimo elemento", ringList.darSiguiente());

	}

	public void testGetPrevious()
	{
		setupEscenario2();
		if(ringList.darElementoPosicionActual().equals(ringList.darUltimo()))
			assertFalse("No se puede retroceder porque es el primer elemento", ringList.darAnterior());


        assertEquals( "La lista  debe crearse vac�a.", "Route1", ringList.darElementoPosicionActual() );
	

	
	public void testLocalizarUltimo()
	{
		setupEscenario2();
		assertEquals("El ultimo no es el esperado", "Route3", ringList.darultimo().darItem());
	}

}
