package testData_structures;

import junit.framework.TestCase;
import model.data_structures.DoubleLinkedList;

public class testDoubleLinkedList<T> extends TestCase {
	
private DoubleLinkedList<T> lista;


	
	public void setupEscenario1()
	{
		lista= new DoubleLinkedList<T>();
		
	}
	
	public void setupEscenario2()
	{
		try
		{
			lista = new DoubleLinkedList<T>();

			T primero = (T) "Route1";
			T segundo = (T) "Route2";
			T tercero =  (T) "Route3";


			lista.agregarElementoFinal(primero);
			lista.agregarElementoFinal(segundo);
			lista.agregarElementoFinal(tercero);


		}
		catch( Exception e )
		{
			fail("No deber�a lanzar excepci�n.");
		}

	}

	public void testListaDoble( )
	{
		setupEscenario1();
		assertEquals( "La lista debe encontrarse vac�a.", 0, lista.darNumeroElementos() );
		
		setupEscenario2( );

		assertEquals( "La lista no se encuentra vac�a.", 3, lista.darNumeroElementos() );
		assertNotNull( "La lista deber�a tener un primer elemento.", lista.darElemento(0));
		assertEquals( "El primer elemento no es el esperado.", "Route1", lista.darElemento(0));
		assertEquals( "El segundo elemento no es el esperado.", "Route2", lista.darElemento(1));
		assertEquals( "El ultimo elemento no es el esperado.", "Route3", lista.darUltimo().darItem());

	}

	public void testElementoPosicionActual()
	{		
		setupEscenario2();
		assertEquals( "El elemento no es el de la posicion actual", "Route1", lista.darElementoPosicionActual() );
	}

	public void testDarElemento()
	{
		setupEscenario2();
		assertEquals("El elemento no es el esperado", "Route2", lista.darElemento(1));
	}

	public void testDarNumeroElementos()
	{
		setupEscenario1();
		assertEquals("La lista se encuentra vacia", 0, lista.darNumeroElementos());
		
		setupEscenario2();
		assertEquals("El tama�o no es el esperado", 3, lista.darNumeroElementos());
	}

	public void testGetNext()
	{
		setupEscenario2();
		if(lista.darElementoPosicionActual().equals(lista.darUltimo()))
			assertFalse("No se puede avanzar porque es el ultimo elemento", lista.darSiguiente());

	}

	public void testGetPrevious()
	{
		setupEscenario2();
		if(lista.darElementoPosicionActual().equals(lista.darUltimo()))
			assertFalse("No se puede retroceder porque es el primer elemento", lista.darSiguiente());

	}
	
	public void testDarUltimo()
	{
		setupEscenario2();
		assertEquals("El ultimo no es el esperado", "Route3", lista.darUltimo().darItem());
	}
}
	

